var c = require('./js-combinator.js');

// here, "no" means "argument not provided at all"
var id = ['id'];
var keys = ['1key','2keys'];
var plans = ['no','plan'];
var sys = ['no','true','false'];

function writeTest(id, keys, plans, sys) {
	var code = writeTestCode.apply(null, arguments);
	console.log(code);
}

function describeTest(id, keys, plans, sys) {
	var dsc = "PUTs ";

	dsc += ({
		'no': 'all keys ',
		'1key': 'one named key ',
		'2keys': 'two named keys '
	})[keys];

	dsc += ({
		'no': '',
		'plan': 'with planUid '
	})[plans];

	dsc += ({
		'no': '',
		'true': 'with systemInitiated true ',
		'false': 'with systemInitiated false '
	})[sys];

	dsc += 'for an Org';

	return "'" + dsc.trim() + "'";
}

function getArgs(id, keys, plans, sys) {
	var arglist = [id, keys, plans, sys];

	// remove all trailing "missing" and "skip" args
	while(arglist[arglist.length-1] == 'no') arglist.pop();

	// replace all remaining "no" arguments with "null"
	arglist = arglist.map(function(v,i,a) { return v=='no'?'null':v; });

	// convert arg-descriptors into usable values
	arglist = arglist.map(function(v,i,a) {
		switch(v) {
			case 'id': return 700;
			case '1key': return '{"key":"key1name", "value":"key1value"}';
			case '2keys': return '[{"key":"key1name", "value":"key1value"},{"key":"key2name", "value":"key2value"}]';
			case 'plan': return '"8abd0d4"';
			default: return v;
		}
	});

	return arglist;
}

function getHashUrl(id, keys, plan) {

	var url = 'OrgSettings/' + id.value;

	if(plan['case'] != 'no') {
		url += '?';
		/*
		if(keys['case'] != 'no') {

			url += 'keys=' + [].concat(JSON.parse(keys.value)).map(function getKeyName(key) {
				return key.key;
			}).join(',');

		}
		*/
		url += 'planUid=' + JSON.parse(plan.value).toString();

	}

	return url;
}

function getHashPacket(keys) {
	var args = [
		{ key: 'key1name', value: 'key1value', locked: false },
		{ key: 'key2name', value: 'key2value', locked: false }
	];

	switch(keys.case) {
		case '1key':
			return JSON.stringify(args.slice(0, 1));
			break;

		case '2keys':
			return JSON.stringify(args);
			break;
	}
}

function getHashSys(sys) {
	if(sys['case'] == 'no') return 'jasmine.undefined';
	return sys.value;
}

function writeTestCode(id, keys, plan, sys) {
	var testDescription = describeTest.apply(null, arguments);
	var arglist = getArgs.apply(null, arguments);

	var urlHash = getHashUrl({'case': id, 'value': arglist[0]}, {'case': keys, 'value': arglist[1]}, {'case': plan, 'value': arglist[2]});
	var packetHash = getHashPacket({'case': keys, 'value': arglist[1]});
	var sysHash = getHashSys({'case': sys, 'value': arglist[3]});

	return	"it(" + testDescription + ", function() {\n" +
			"	expect(cp.setting.Org.put(" + arglist + ")).sentServiceArgs({\n" +
			"		'type': 'PUT',\n" +
			"		'url': '" + urlHash + "',\n" +
			"		'data': JSON.stringify({\n" +
			"			'packets':" + packetHash + "\n" +
			"		}),\n" +
			"		'systemInitiated': " + sysHash + "\n" +
			"	});\n" +
			"});";

}


Array.prototype.toArgument = function() {
	return '[' + this.toString() + ']';
};

c.combinate([id, keys, plans, sys], writeTest);

console.log();
