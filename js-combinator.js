/**
 * combinate
 * @desc given several sets of choices, combinate will walk through every possible combination, calling a provided function each time with the selected choices
 * @param choices	an array of arrays; each sub-array should present a set of alternatives from which one should be chosen
 * @param callback	a function that will be called for each combination, and will receive the choices; it will receive one argument per sub-array
 * @example
 	combinate(
 		[
	 		['short','tall'],
	 		['infant','child','teen','adult','old'],
	 		['man','woman']
 		], function(height, age, sex) { console.log('a ' + height + ' ' + age + ' ' + sex); }
 	);
 */

function combinate(choices, callback) {

	// generate trackers based on provided choices
	var trackers = [];
	for(var i = 0, iMax = choices.length; i < iMax; i++) {
		trackers.push(0);
	}

	// for a given tracker index, increment the tracker if possible, or reset it and indicate overflow
	function advanceTracker(idx) {

		// compare tracker position to its trange
		var position = trackers[idx]; // e.g.: DELETE = 2
		var max = choices[idx].length - 1; // max = 3

		// if tracker hasn't reached its max, advance it
		if(position < max) {
			trackers[idx]++;
			return false;
		}

		// otherwise, reset tracker and return true (indicating overflow to next tracker)
		trackers[idx] = 0;
		return true;
	}

	// walking algorithm
	var iterations = 0;
	while(true) {

		// construct array of current choices
		var current = [];
		for(var i = 0, iMax = choices.length; i < iMax; i++) {
			current.push(choices[i][trackers[i]]);
		}

		var callbackReturn = callback.apply(trackers.slice(), current);
		iterations++;
		if(callbackReturn === false) break;

		// loop through all trackers in reverse order, incrementing them until one indicates it hasn't overflowed (by returning false)
		// if still overflow at end of all trackers, we're done!
		var idx = choices.length - 1;
		while( idx >= 0 && advanceTracker(idx) ) { idx--; }
		if(idx === -1) break;
	}

	return iterations;
}

// for nodejs
if(module) exports.combinate = combinate;
