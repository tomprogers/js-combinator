Combinatoric callback algorithm in javascript
==




## About

js-combinator is a single function that executes a user-defined callback once for every possible combination of a set of options.

For example, suppose we wish to perform some work for every possible combination of age, height, and sex.

We can express these attributes like so:

    ages = ['young', 'adult', 'old'];
	heights = ['short', 'tall'];
	sexes = ['male', 'female'];

Let's also define a function that does the work we care about. Somehow, we'll figure out a way to provide this function with a single age, height, and sex from our set of choices.

	function theWork(age, height, sex) {
		console.log('I am a ' + age + ', ' + height + ' ' + sex + '.');
	}

We can present the lists of attributes and our work function to js-combinator's **combinate** function, and it will traverse every combination (in a deterministic order), calling theWork each time with a different set of options.

	combinate([ages, heights, sexes], theWork);

The result:

	I am a young, short male.
	I am a young, short female.
	I am a young, tall male.
	I am a young, tall female.
	I am a adult, short male.
	I am a adult, short female.
	I am a adult, tall male.
	I am a adult, tall female.
	I am a old, short male.
	I am a old, short female.
	I am a old, tall male.
	I am a old, tall female.



## Usage

	combinate( choiceArray, callbackFunction )

Where:

**choiceArray** is an array of arrays to be iterated over

**callbackFunction** is a function that will receive the choices as individual arguments; if it returns **false**, combinate will immediately terminate

combinate returns the number of iterations that were executed (useful for partial walks).




## Copyright and licensing

Copyright 2014 Tom Rogers

js-combinator is available under the terms of the GPL v3 (see GPL3.txt for details).




## TODO

* I guess... wait for feedback or profits or something.